package kstreams;

import java.time.Duration;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;

public class Consumer {
    public static void main(String[] args) {
        var consumer = createConsumer();
        var timer = new Timer();
        var topic = "aggregated";
        consumer.subscribe(List.of(topic));

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                var records = consumer.poll(Duration.ofSeconds(1));
                for (var record : records) {
                    System.out.println(record.value());
                }
                System.out.println("Read once");
            }
        }, 0, 2000);
    }

    private static KafkaConsumer<String, Integer> createConsumer() {
        var bootstrapServers = "localhost:9092";
        var groupId = "group1";

        var properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.setProperty(
            ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(
            ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, IntegerDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");

        return new KafkaConsumer<>(properties);
    }
}
