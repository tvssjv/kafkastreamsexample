package kstreams;

import java.util.Properties;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

public class Producer {
    public static void main(String[] args) {
        var kafkaProducer = createKafkaProducer();
        var timer = new Timer();
        var random = new Random();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                var num = random.nextInt(1, 30);
                num = 100;
                var record = new ProducerRecord<String, Integer>("temp", num);
                System.out.println(num);
                kafkaProducer.send(record);
            }
        }, 0, 1000);
    }

    private static KafkaProducer<String, Integer> createKafkaProducer() {
        var properties = new Properties();
        var bootstrapServers = "localhost:9092";
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.setProperty(
            ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(
            ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class.getName());
        return new KafkaProducer<>(properties);
    }
}
